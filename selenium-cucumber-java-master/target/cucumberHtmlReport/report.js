$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("createuser.feature");
formatter.feature({
  "line": 2,
  "name": "Validate new user creation",
  "description": "",
  "id": "validate-new-user-creation",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 4,
  "name": "Validae new user is created successfully",
  "description": "",
  "id": "validate-new-user-creation;validae-new-user-is-created-successfully",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "I launch the application",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I fill the registration form",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "I validate that user is created successfully",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "I close the browser",
  "keyword": "And "
});
formatter.match({
  "location": "steps.i_launch_the_application()"
});
formatter.result({
  "duration": 71185241607,
  "status": "passed"
});
formatter.match({
  "location": "steps.i_fill_the_registration_form()"
});
formatter.result({
  "duration": 17740340634,
  "error_message": "java.lang.NullPointerException\r\n\tat info.seleniumcucumber.userStepDefintions.steps.createaccount(steps.java:158)\r\n\tat info.seleniumcucumber.userStepDefintions.steps.i_fill_the_registration_form(steps.java:43)\r\n\tat ✽.And I fill the registration form(createuser.feature:6)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "steps.validate_account_created_successfully()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "steps.i_close_the_browse()"
});
formatter.result({
  "status": "skipped"
});
formatter.uri("login.feature");
formatter.feature({
  "line": 1,
  "name": "Validate user loggin",
  "description": "",
  "id": "validate-user-loggin",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "Validae that user can login successfully",
  "description": "",
  "id": "validate-user-loggin;validae-that-user-can-login-successfully",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "I launch the application",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "I enter valid \"\u003cusername\u003e\" and \"\u003cpassword\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 6,
  "name": "I validate that user is logged in successfully",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "I close the browser",
  "keyword": "And "
});
formatter.examples({
  "line": 8,
  "name": "",
  "description": "",
  "id": "validate-user-loggin;validae-that-user-can-login-successfully;",
  "rows": [
    {
      "cells": [
        "username",
        "passwword"
      ],
      "line": 9,
      "id": "validate-user-loggin;validae-that-user-can-login-successfully;;1"
    },
    {
      "cells": [
        "abc.test@gmail.com",
        "test123@"
      ],
      "line": 10,
      "id": "validate-user-loggin;validae-that-user-can-login-successfully;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 10,
  "name": "Validae that user can login successfully",
  "description": "",
  "id": "validate-user-loggin;validae-that-user-can-login-successfully;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "I launch the application",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "I enter valid \"abc.test@gmail.com\" and \"\u003cpassword\u003e\"",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 6,
  "name": "I validate that user is logged in successfully",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "I close the browser",
  "keyword": "And "
});
formatter.match({
  "location": "steps.i_launch_the_application()"
});
formatter.result({
  "duration": 23112131939,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "abc.test@gmail.com",
      "offset": 15
    },
    {
      "val": "\u003cpassword\u003e",
      "offset": 40
    }
  ],
  "location": "steps.login(String,String)"
});
formatter.result({
  "duration": 8402146193,
  "status": "passed"
});
formatter.match({
  "location": "steps.i_validate_that_user_is_created_successfully()"
});
formatter.result({
  "duration": 532582836,
  "status": "passed"
});
formatter.match({
  "location": "steps.i_close_the_browse()"
});
formatter.result({
  "duration": 4815749733,
  "status": "passed"
});
formatter.uri("productcheckout.feature");
formatter.feature({
  "line": 1,
  "name": "Validate User can checkout the product successfully",
  "description": "",
  "id": "validate-user-can-checkout-the-product-successfully",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "Validate user can add item to cart",
  "description": "",
  "id": "validate-user-can-checkout-the-product-successfully;validate-user-can-add-item-to-cart",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "I launch the application",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "I enter valid \"\u003cusername\u003e\" and \"\u003cpassword\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 6,
  "name": "I click on \"\u003ccategory\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "I add item to cart",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Validate product is added successfully",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "I close the browser",
  "keyword": "And "
});
formatter.examples({
  "line": 10,
  "name": "",
  "description": "",
  "id": "validate-user-can-checkout-the-product-successfully;validate-user-can-add-item-to-cart;",
  "rows": [
    {
      "cells": [
        "category",
        "username",
        "password"
      ],
      "line": 11,
      "id": "validate-user-can-checkout-the-product-successfully;validate-user-can-add-item-to-cart;;1"
    },
    {
      "cells": [
        "T-SHIRTS",
        "abc.test@gmail.com",
        "test23@"
      ],
      "line": 12,
      "id": "validate-user-can-checkout-the-product-successfully;validate-user-can-add-item-to-cart;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 12,
  "name": "Validate user can add item to cart",
  "description": "",
  "id": "validate-user-can-checkout-the-product-successfully;validate-user-can-add-item-to-cart;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "I launch the application",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "I enter valid \"abc.test@gmail.com\" and \"test23@\"",
  "matchedColumns": [
    1,
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 6,
  "name": "I click on \"T-SHIRTS\"",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "I add item to cart",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Validate product is added successfully",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "I close the browser",
  "keyword": "And "
});
formatter.match({
  "location": "steps.i_launch_the_application()"
});
formatter.result({
  "duration": 19253675366,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "abc.test@gmail.com",
      "offset": 15
    },
    {
      "val": "test23@",
      "offset": 40
    }
  ],
  "location": "steps.login(String,String)"
});
formatter.result({
  "duration": 6321743578,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "T-SHIRTS",
      "offset": 12
    }
  ],
  "location": "steps.Click_On_Categgory(String)"
});
formatter.result({
  "duration": 3128679967,
  "status": "passed"
});
formatter.match({
  "location": "steps.Add_Item_To_Cart()"
});
formatter.result({
  "duration": 679043588,
  "status": "passed"
});
formatter.match({
  "location": "steps.validate_product_added_successfully()"
});
formatter.result({
  "duration": 4243754253,
  "status": "passed"
});
formatter.match({
  "location": "steps.i_close_the_browse()"
});
formatter.result({
  "duration": 2401067104,
  "status": "passed"
});
});