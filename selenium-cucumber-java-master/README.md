### selenium-cucumber-java

### Installation (pre-requisites)
1. JDK 1.8+ (make sure Java class path is set)
2. Maven (make sure .m2 class path is set)
3. Eclipse
4. Eclipse Plugins for
    - Maven
    - Cucumber
5. Browser driver (make sure you have your desired browser driver and class path is set)

### Framework set up
Fork / Clone repository from [here]( https://sravanthiy@bitbucket.org/sravanthiy/selenium-cucumber-java) or download zip and set it up in your local workspace.
