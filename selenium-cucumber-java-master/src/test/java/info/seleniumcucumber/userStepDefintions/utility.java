package info.seleniumcucumber.userStepDefintions;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;


public class utility {
	
	WebDriver driver;
	
	
	
public  void  createAnAccount_Title(String inputTitle){
		
		switch  (inputTitle){
		
		case "Mr":
			try{
		driver.findElement(By.id("id_gender1")).click();
		}
			catch(Exception e){
				System.out.println("Title:Element not  found");
			}
		break;
		case  "Mrs":
		driver.findElement(By.id("id_gender2")).click();
		break;
		default:
			System.out.println("Please pass the right Title");
			break;
		}

	}


public void createaccount(String email,String gender, String firstname, String lastname, String password, String days, String months, String years, String company, String address1, String city, String state, String postcode, String country, String mobile)
{
	driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
	driver.findElement(By.xpath("//a[@class='login']")).click();
	driver.findElement(By.id("email_create")).sendKeys("email");
	driver.findElement(By.id("SubmitCreate")).click();
	// Validate that Registration form is displayed or not
	String pagesubheading = driver.findElement(By.xpath("//h3[@class='page-subheading']")).getText();
	Assert.assertEquals("Registration successful", "CREATE AN ACCOUNT", pagesubheading);
	
	//driver.findElement(By.id("id_gender2")).click();
	//createAnAccount_Title(gender);
	driver.findElement(By.id("customer_firstname")).sendKeys(firstname);
	driver.findElement(By.id("customer_lastname")).sendKeys(lastname);
	driver.findElement(By.id("passwd")).sendKeys(password);
	Select day = new Select(driver.findElement(By.id("days")));
	day.selectByValue(days);
	
	Select month = new Select(driver.findElement(By.id("months")));
	month.selectByValue(months);
	
	Select year = new Select(driver.findElement(By.id("years")));
	year.selectByValue(years);
	
	driver.findElement(By.id("company")).sendKeys(company);
	driver.findElement(By.id("address1")).sendKeys(address1);
	
	driver.findElement(By.id("city")).sendKeys(city);
	Select stateobj = new Select(driver.findElement(By.id("id_state")));
	stateobj.selectByVisibleText(state);
	driver.findElement(By.id("postcode")).sendKeys(postcode);
	Select countryobj = new Select(driver.findElement(By.id("id_country")));
	countryobj.selectByVisibleText(country);
	driver.findElement(By.id("phone_mobile")).sendKeys(mobile);
	
	driver.findElement(By.id("submitAccount")).click();
}
}