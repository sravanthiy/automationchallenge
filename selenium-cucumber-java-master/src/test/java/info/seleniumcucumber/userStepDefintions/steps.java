package info.seleniumcucumber.userStepDefintions;


import java.util.concurrent.TimeUnit;

import org.junit.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class steps {

	WebDriver driver;
	String pagesubheading;
	String Itemdescription;
	utility util = new utility();
	

	@Given("^I launch the application$")
	public void i_launch_the_application()
	{
		System.setProperty("webdriver.chrome.driver", "E:\\Tools\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://automationpractice.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(1000, TimeUnit.SECONDS);
		System.out.println("Driver launched successfully//");
		
	}
	
	@And("I fill the registration form")
	public void i_fill_the_registration_form()
	{
		
		createaccount("abc.test20@gmail.com","Mrs","automation","test","test123@","10","8","1990","ABC","mainroad","Manchester","Texas","12345","United States","012356789");
	}
	
	@Then("I validate that user is created successfully") 
	public void validate_account_created_successfully()
	{
		
	String Pageheading =  driver.findElement(By.xpath("//h1[@class='page-heading']")).getText();
	String accountinfo = driver.findElement(By.xpath("//p[@class='info-account']")).getText();
	Assert.assertEquals("Account Created Successfully", "MY ACCOUNT", Pageheading);
	Assert.assertEquals("Account Created Successfully", "Welcome to your account. Here you can manage all of your personal information and orders.", accountinfo);	
	}
	
	@And("I close the browser")
	public void i_close_the_browse()
	{
		driver.quit();
	}
	
	@And("^I enter valid \"([^\"]*)\" and \"([^\"]*)\"$")
	public void login(String username, String password)
	{
		driver.findElement(By.xpath("//a[@class='login']")).click();
		driver.findElement(By.id("email")).sendKeys(username);
		driver.findElement(By.id("passwd")).sendKeys("test123@");
		driver.findElement(By.id("SubmitLogin")).click();
	}
	
	@Then("I validate that user is logged in successfully")
	public void i_validate_that_user_is_created_successfully()
	{
		String Pageheading =  driver.findElement(By.xpath("//h1[@class='page-heading']")).getText();
		String accountinfo = driver.findElement(By.xpath("//p[@class='info-account']")).getText();
		Assert.assertEquals("User logged in successfully", "MY ACCOUNT", Pageheading);
		//Assert.assertEquals(accountinfo, "Welcome to your account. Here you can manage all of your personal information and orders.", "User logged in successfully");	
	}

	@And("^I click on \"([^\"]*)\"$")
	public void Click_On_Categgory(String Category)
	{
		driver.findElement(By.linkText("T-SHIRTS")).click();
	}
	@And("I add item to cart")
	public void Add_Item_To_Cart()
	{
		WebElement item = driver.findElement(By.xpath("//img[@title='Faded Short Sleeve T-shirts']"));
		Itemdescription = item.getCssValue("title");
		Actions a = new Actions(driver);
		a.moveToElement(item).build().perform();
		driver.findElement(By.xpath("//a[@class='button ajax_add_to_cart_button btn btn-default']")).click();
	}
	@Then("Validate product is added successfully")
	public void validate_product_added_successfully() throws InterruptedException
	{
	
		driver.manage().timeouts().implicitlyWait(10000, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//div[@class='layer_cart_product col-xs-12 col-md-6']//span[@class='cross']")).click();
		driver.findElement(By.xpath("//a[@title='View my shopping cart']")).click();
		WebElement prodname = driver.findElement(By.xpath("//p[@class='product-name']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", prodname);
		Thread.sleep(500); 
		String productdescription = driver.findElement(By.xpath("//p[@class='product-name']")).getText();
		System.out.println(productdescription);
//		Assert.assertEquals(productdescription, Itemdescription, "Correct Prouct added");
		Assert.assertEquals("correct product added", Itemdescription, productdescription);
	}
	
	@And("I click on cart")
	public void i_click_on_cart()
	{
		driver.findElement(By.xpath("//a[@title='View my shopping cart']")).click();
		
	}
	
	@Then("Validate correct item is added")
	public void validate_correct_product_added()
	{
		String productdescription = driver.findElement(By.xpath("//[@class='product-name']")).getText();
		Assert.assertEquals(productdescription, Itemdescription, "Correct Prouct added");
	}
	
public  void  createAnAccount_Title(String inputTitle){
		
		switch  (inputTitle){
		
		case "Mr":
			try{
		driver.findElement(By.xpath("//input[@id='id_gender1']")).click();
		}
			catch(Exception e){
				System.out.println("Title:Element not  found");
			}
		break;
		case  "Mrs":
		driver.findElement(By.xpath("//input[@id='id_gender2']")).click();
		break;
		default:
			System.out.println("Please pass the right Title");
			break;
		}

	}


public void createaccount(String email,String gender, String firstname, String lastname, String password, String days, String months, String years, String company, String address1, String city, String state, String postcode, String country, String mobile)
{	
driver.findElement(By.xpath("//a[@class='login']")).click();
	driver.findElement(By.id("email_create")).sendKeys(email);
	driver.findElement(By.id("SubmitCreate")).click();
	// Validate that Registration form is displayed or not
	String pagesubheading = driver.findElement(By.xpath("//h3[@class='page-subheading']")).getText();
	Assert.assertEquals("Registration form displayed", "CREATE AN ACCOUNT", pagesubheading);
//	Actions action = new Actions(driver);
//	WebElement we = driver.findElement(By.id("customer_firstnamev"));
//	action.moveToElement(we).click().build().perform();
	driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
	driver.findElement(By.xpath("//form[@id='account-creation_form']//input[@id='id_gender1']")).click();
	
	//driver.findElement(By.id("customer_firstname")).sendKeys("sda");
	//driver.findElement(By.xpath("//input[@id='id_gender2']")).click();
	WebElement  element  = driver.findElement(By.xpath("//input[@id='id_gender2']"));
	JavascriptExecutor  executor  =  (JavascriptExecutor)driver;
	executor.executeScript("arguments[0].click()", element);
//	boolean b  = driver.findElement(By.xpath("//input[@id='id_gender2']")).isDisplayed();
//	System.out.println(b);
//	driver.findElement(By.xpath("//input[@id='id_gender2']")).click();
	//createAnAccount_Title(gender);
	driver.findElement(By.id("customer_firstname")).sendKeys(firstname);
	driver.findElement(By.id("customer_lastname")).sendKeys(lastname);
	driver.findElement(By.id("passwd")).sendKeys(password);
	Select day = new Select(driver.findElement(By.id("days")));
	day.selectByValue(days);
	
	Select month = new Select(driver.findElement(By.id("months")));
	month.selectByValue(months);
	
	Select year = new Select(driver.findElement(By.id("years")));
	year.selectByValue(years);
	
	driver.findElement(By.id("company")).sendKeys(company);
	driver.findElement(By.id("address1")).sendKeys(address1);
	
	driver.findElement(By.id("city")).sendKeys(city);
	Select stateobj = new Select(driver.findElement(By.id("id_state")));
	stateobj.selectByVisibleText(state);
	driver.findElement(By.id("postcode")).sendKeys(postcode);
	Select countryobj = new Select(driver.findElement(By.id("id_country")));
	countryobj.selectByVisibleText(country);
	driver.findElement(By.id("phone_mobile")).sendKeys(mobile);
	
	driver.findElement(By.id("submitAccount")).click();
	System.out.println("branch2");
}
}