Feature: Validate User can checkout the product successfully

Scenario Outline: Validate user can add item to cart
Given I launch the application
And I enter valid "<username>" and "<password>"
And I click on "<category>"
And I add item to cart
Then Validate product is added successfully
And I close the browser
Examples:
|category|username|password|
|T-SHIRTS|abc.test@gmail.com|test23@|

#Scenario Outline: Validate correct item is added to cart
#Given I launch the application
#And I enter valid "<username>" and "<password>"
#And I click on cart
#Then Validate correct item is added
#And I close the browser
#Examples:
#|username|password|
#|abc.test@gmail.com|test123@|